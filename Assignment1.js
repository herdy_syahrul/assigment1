import React, { Component } from 'react'
import {Text, View, StyleSheet, Image, Button, ScrollView, TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5';

export class Assigntment1 extends Component {
    constructor(){
        super()
        this.state = {
            title:'Avengers',
            data: []
        }
    }

    // showData = () => {
    //     fetch('http://www.omdbapi.com/?s=avengers&apikey=997061b4&')
    //     .then(response => response.json())
    //     .then(json => this.setState({data:json}))
    // }

    //    componentDidMount = () => {
    //     fetch('http://www.omdbapi.com/?s=avengers&apikey=997061b4&')
    //     .then(data =>  data.json())
    //     .then(json =>  this.setState({data:json.Search}))
    // }

      async componentDidMount() {
        try {
            const response = await fetch(`http://www.omdbapi.com/?s=avengers&apikey=997061b4&`);
            const json = await response.json();
            this.setState({data:json.Search})
        } catch (error) {
            console.log('data salah');
        }
      }

    render() {
        return (
            <ScrollView>
            <View style={styles.container}>
             <View style={{flexDirection:'column', flex:1, backgroundColor:'#312C51'}}>
              <View style={styles.header}>
                <Text style={{marginHorizontal:10, color:'violet', fontWeight:'normal', fontSize:25}}>List of movies</Text>
                <Icon name="film" size={20} style={{backgroundColor:'violet', padding:5, borderRadius:10}}/>
              </View>

              {this.state.data.map((value,index) => (
               <View key={index} style={{ flex:1, flexDirection:'row', padding:10, marginVertical:10, elevation:5,
                        borderWidth:0.6, borderRightColor:'violet', backgroundColor:'#48426D'}}>
                <View style={{flex:1, justifyContent:'center', alignItems:'center', padding:5}}>
                    <Image style={{height:70, elevation:8, width:80, marginRight:20, borderWidth:1.5, borderColor:'violet', borderRadius:25}} source={{uri:value.Poster}} />
                </View>
                 <View style={{flex:3, flexDirection:'column', padding:5,}}>
                    <Text key={index} style={{ fontSize:23, fontWeight:'normal', color:'white'}}>{value.Title}</Text>
                    <Text style={{ fontSize:15, marginVertical:5, color:'lightgrey'}}>imdbID : {value.imdbID}</Text>
                    <TouchableOpacity style={styles.btn} onPress={() => this.showData()}>
                      <Text key={index} style={{color:'violet', textAlign:'center'}}>{value.Year}</Text>
                    </TouchableOpacity>
                </View>
              </View>
              ))}
             </View>
            </View>
            </ScrollView>
        )
    }
}

export default Assigntment1

const styles = StyleSheet.create({
    container: {
       flex:1,
       flexDirection:'row',
    },
    header: {
        flexDirection:'row', 
        height:60, 
        justifyContent:'center', 
        backgroundColor:'#312C51', 
        borderRadius:20, 
        alignItems:'center', 
        margin:15,
        borderColor: "violet",
        borderWidth: 1,
        elevation:5
    },
    btn: {
       borderWidth: 1,
       backgroundColor:'#312C51',
       padding: 2,
       width:100,
       borderRadius: 100,
       borderColor: "violet",
       borderWidth: 1,
       marginVertical: 5,
       elevation: 5
    }
})
